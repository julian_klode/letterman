#!/usr/bin/python
#
# Copyright (C) 2010-2014 Julian Andres Klode <jak@jak-linux.org>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
from tempfile import NamedTemporaryFile
import gobject, gtk, pango

try:
    import bugbuddy
    bugbuddy.install()
except ImportError:
    pass

try:
    import gtkspell
except ImportError:
    gtkspell = None
gtkspell = None

import locale, subprocess, os, sys, textwrap, errno, re


CONFIG_DIR = os.path.join(os.getenv("HOME"), ".letterman")
LATEX_ESCAPE = "[&{}]"

def smtime(filename):
    try:
        return os.path.getmtime(filename)
    except OSError, (error, message):
        if error == errno.ENOENT: return 0
        raise e

class LaTeXBuffer(gtk.TextBuffer):

    def __init__(self):
        gtk.TextBuffer.__init__(self)
        self._tags = {}
        self.register_deserialize_format("text/x-tex", self._deserialize)
        self.register_serialize_format("text/x-tex", self._serialize)

    def create_tag(self, name, **kwds):
        self._tags[name] = gtk.TextBuffer.create_tag(self, name, **kwds)
        return self._tags[name]

    def _deserialize(self, register_buf, content_buf, iter, data, create_tags):
        s = data.decode("utf-8").strip()
        tags = []
        tokens = re.findall(r"\\([a-z]+)\{|(\})|(\n\n+)|(\\\\)|" +
                            r"\\(%s)|(.)" % LATEX_ESCAPE, s, re.DOTALL)

        for tag, end, par, newline, escaped, other in tokens:
            if tag:
                tags.append((tag, content_buf.get_end_iter().get_offset()))
            if end:
                identifier, start = tags.pop()
                start = content_buf.get_iter_at_offset(start)
                end = content_buf.get_end_iter()
                content_buf.apply_tag_by_name(identifier, start, end)
            if par:
                content_buf.insert_at_cursor("\n\n")
            if newline:
                content_buf.insert_at_cursor("\n")
            if escaped:
                content_buf.insert_at_cursor(escaped)
            if other:
                content_buf.insert_at_cursor(other.replace("\n", " "))

        return True

    def _serialize(self, register_buf, content_buf, start, end):
        s_offset = start.get_offset()
        e_offset = end.get_offset()
        s = ""
        copen = 0
        tags = []
        content_buf.get_tag_table().foreach(lambda tag, _: tags.append(tag))
        i = s_offset
        while i < e_offset:
            iter = content_buf.get_iter_at_offset(i)
            char = iter.get_char()

            if char == '\n' and i + 1 < e_offset:
                next_iter = content_buf.get_iter_at_offset(i + 1)
                next_char = next_iter.get_char()
                if next_char == '\n':
                    char = "\n\n"
                    i += 1
                if next_char != '\n':
                    char = "\\\\\n" + next_char
                    i += 1
            for tag in tags:
                if iter.begins_tag(tag):
                    s += "\\%s{" % tag.get_property("name")
                    copen += 1
            for tag in tags:
                if iter.ends_tag(tag):
                    s += "}"
                    copen -= 1
            s += char
            i += 1
        s += "}" * copen
        return s

class Letterman(object):

    def __init__(self):
        builder = gtk.Builder()
        builder.set_translation_domain("")

        while os.path.islink(sys.argv[0]):
            target = os.readlink(sys.argv[0])
            if target[0] != '/':
                sys.argv[0] = os.path.join(os.path.dirname(sys.argv[0]),
                                           target)
        builder.add_from_file(os.path.join(os.path.dirname(sys.argv[0]),
                                           "letterman.ui"))

        self.modified = False
        self._window = builder.get_object("window")
        self._adresse = builder.get_object("addresse")
        self._betreff = builder.get_object("betreff")
        self._anrede = builder.get_object("anrede")
        self._inhalt = builder.get_object("inhalt")
        self._about = builder.get_object("aboutdialog")
        self._save = builder.get_object("savedialog")
        self._open = builder.get_object("opendialog")

        self._inhalt.set_buffer(LaTeXBuffer())

        if gtkspell is not None:
            gtkspell.Spell(self._adresse)
            gtkspell.Spell(self._inhalt)
        builder.get_object("mhelp-about").connect("activate", self.about)
        builder.get_object("view").connect("activate", self.view)
        builder.get_object("save").connect("activate", self.save)
        builder.get_object("save-as").connect("activate", self.save, True)
        builder.get_object("open").connect("activate", self.open)
        builder.get_object("new").connect("activate", self.new)
        builder.get_object("mquit").connect("activate", gtk.main_quit)

        self._adresse.get_buffer().connect("changed", self.on_changed)
        self._betreff.connect("changed", self.on_changed)
        self._anrede.connect("changed", self.on_changed)
        self._inhalt.get_buffer().connect("changed", self.on_changed)
        self.cursor_change = False
        self._buffer = self._inhalt.get_buffer()
        self._tags = dict()
        self._atags = dict()
        self.__add_tag(builder, "textbf", weight=pango.WEIGHT_BOLD)
        self.__add_tag(builder, "textit", style=pango.STYLE_ITALIC)
        self.__add_tag(builder, "underline", underline=pango.UNDERLINE_SINGLE)
        self._inhalt.set_wrap_mode(gtk.WRAP_WORD)

        self._buffer.connect("notify::cursor-position", self.on_cursor_change)

    def __add_tag(self, builder, name, **kwds):
        builder.get_object(name).connect("toggled", self.apply_tag, name)
        self._tags[name] = self._buffer.create_tag(name, **kwds)
        self._atags[name] = builder.get_object(name)

    def on_cursor_change(self, *a):

        selection = self._buffer.get_selection_bounds()
        if not selection:
            return
        self.cursor_change = True

        for tag in self._tags:
            active = False
            offset = selection[0].get_offset()
            assert isinstance(offset, int)
            assert isinstance(selection[1].get_offset(), int)
            while offset < selection[1].get_offset():
                i = self._buffer.get_iter_at_offset(offset)
                active |= bool(i.has_tag(self._tags[tag]))
                offset += 1
            self._atags[tag].set_active(active)
        self.cursor_change = False

    def apply_tag(self, toggle, tagname):
        if self.cursor_change:
            return
        selection = self._buffer.get_selection_bounds()
        if not selection:
            return
        if not toggle.get_property("active"):
            self._buffer.remove_tag_by_name(tagname, *selection)
        else:
            self._buffer.apply_tag_by_name(tagname, *selection)

        self.on_changed()

    def about(self, *_):
        self._about.run()
        self._about.hide()

    def on_changed(self, *_):
        if self.modified is not None:
            self.modified = True
            self.set_title()

    def set_title(self):
        if self.modified and not self._window.get_title().startswith("*"):
            self._window.set_title("*" + self._window.get_title())
        elif not self.modified and self._window.get_title().startswith("*"):
            self._window.set_title(self._window.get_title()[1:])

    def new(self, _=None):
        self.modified = None
        self.filename = None
        self._adresse.get_buffer().set_text("")
        self._betreff.set_text("")
        self._anrede.set_text("")
        self._inhalt.get_buffer().set_text("")
        self.modified = False

    def save(self, _=None, force=False):
        if self.filename is None or force:
            while self._save.run():
                filename = self._save.vbox.get_children()[1].get_text()
                self._save.hide()
                if filename:
                    self.filename = filename
                    self.write()
                    break
            else:
                self._save.hide()
        else:
            self.write()

    def open(self, _=None):
        combo = self._open.vbox.get_children()[1]
        combo.set_model(gtk.ListStore(gobject.TYPE_STRING))
        for i in os.listdir("."):
            if i.startswith("."):
                continue
            if i.endswith(".tex"):
                combo.append_text(i[:-4])
        if self._open.run():
            self.filename = combo.get_active_text()
            self.modified = None
            self.read()
            self.modified = False
        self._open.hide()

    def show(self):
        self._window.connect("destroy", gtk.main_quit)
        self._window.show()

    def __get_filename(self):
        try:
            return self._filename
        except AttributeError:
            return None

    def __set_filename(self, value):
        self._filename = value
        if value is not None:
            self._window.set_title("%s - Letterman" % value)
        else:
            self._window.set_title("Letterman Briefschreiber")

    filename = property(__get_filename, __set_filename)

    @property
    def adresse(self):
        return self._adresse.get_buffer().get_property("text").replace("\n", "\\\\")
    @property
    def betreff(self):
        return self._betreff.get_buffer().get_property("text")
    @property
    def anrede(self):
        return self._anrede.get_buffer().get_property("text")
    @property
    def inhalt(self):
        return self._inhalt.get_buffer().get_property("text")

    @staticmethod
    def texify(string):
        return string.replace("&", "\&")

    @staticmethod
    def untexify(string):
        return string.replace("\&", "&").replace("\\\\", "\n")

    def read(self, _=None):
        fobj = open("%s.tex" % self.filename)

        start = False
        inhalt = ""

        for line in fobj:
            if line.startswith(r"\Adresse"):
                self._adresse.get_buffer().set_text(self.untexify(line.split("{", 1)[1].split("}")[0].replace(r"\\", "\n")))
            elif line.startswith(r"\Betreff"):
                self._betreff.set_text(self.untexify(line.split("{", 1)[1].split("}")[0]))
            elif line.startswith(r"\Anrede"):
                self._anrede.set_text(self.untexify(line.split("{", 1)[1].split("}")[0]))
            elif line.strip() == r"\end{g-brief}":
                start = False
                self._inhalt.get_buffer().set_text(self.untexify(self.inhalt).strip())
            elif line.startswith(r"\begin{g-brief}"):
                self._inhalt.get_buffer().set_text("")
                start = True
            elif start:
                inhalt += line

        self._buffer.deserialize(self._buffer, "text/x-tex", self._buffer.get_start_iter(),
                                 inhalt)


    def write(self, _=None):
        temp = open("%s.tex" % self.filename, "w")
        print >> temp, r"\include{Vorlage}"
        print >> temp, r"\Adresse{%s}" % self.texify(self.adresse)
        print >> temp, r"\Betreff{%s}" % self.texify(self.betreff)
        print >> temp, r"\Anrede{%s}" % self.texify(self.anrede)
        print >> temp, r"\begin{document}"
        print >> temp, r"\begin{g-brief}"

        data = self._buffer.serialize(self._buffer, "text/x-tex",
                                      self._buffer.get_start_iter(),
                                      self._buffer.get_end_iter())
        paras = re.split("\n\n+", self.texify(data))
        paras = ['\n'.join(textwrap.wrap(p.strip())) for p in paras if p.strip()]
        print >> temp, '\n\n'.join(paras).encode("utf-8")
        print >> temp, r"\end{g-brief}"
        print >> temp, r"\end{document}"
        temp.close()
        self.modified = False
        self.set_title()

    def view(self, _=None):
        if self.filename is None:
            self.save()
        if self.filename is None:
            return
        if self.modified:
            self.save()

        if (smtime("%s.tex" % self.filename) >
            smtime("%s.pdf" % self.filename)):

            env = os.environ.copy()
            env["TEXINPUTS"] = ".:%s:" % CONFIG_DIR

            subprocess.check_call(["pdflatex", "-halt-on-error",
                                   "%s.tex" % self.filename], env=env)

            os.unlink("%s.aux" % self.filename)
            os.unlink("%s.log" % self.filename)
        subprocess.call(["xdg-open", "%s.pdf" % self.filename])

def main():
    locale.setlocale(locale.LC_ALL, '')
    letterman = Letterman()
    briefe = os.path.join(os.getenv("HOME"), "Dokumente", "Briefe")
    if not os.path.exists(briefe):
        os.makedirs(briefe)
    os.chdir(briefe)
    letterman.show()
    gtk.main()

if __name__ == '__main__':
    main()
