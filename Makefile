all:

install:
	install -d $(DESTDIR)/usr/bin
	install -D -m755 letterman.py $(DESTDIR)/usr/share/letterman/letterman.py
	install -D -m644 letterman.ui $(DESTDIR)/usr/share/letterman/letterman.ui
	ln -sf ../share/letterman/letterman.py $(DESTDIR)/usr/bin/letterman
	install -D -m644 letterman.desktop $(DESTDIR)/usr/share/applications/letterman.desktop
